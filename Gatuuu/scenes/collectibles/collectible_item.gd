extends Area2D

@export var _collectible_image: Sprite2D

@onready var _initial_collectible_pos =  _collectible_image.position.y
var _direction = 1

func _process(_delta):
		var _collectible_jitter = 0.1
		
		_collectible_image.position.y += _direction * _collectible_jitter
		if abs(_collectible_image.position.y - _initial_collectible_pos) > 2:
			_direction *= -1

func _on_body_entered(_body):
	if _body is Player:
		PlayerStatus.fish_count += 1
		queue_free()
