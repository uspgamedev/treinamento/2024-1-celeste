extends Area2D

@export var _message_baloon: Sprite2D
@export var _animation: AnimationPlayer

@onready var _initial_baloon_pos =  _message_baloon.position.y
var _direction = 1

func _process(_delta):
	if _message_baloon.is_visible_in_tree() == true:
		var _baloon_jitter = 0.2
		
		_message_baloon.position.y += _direction * _baloon_jitter
		if abs(_message_baloon.position.y - _initial_baloon_pos) > 5:
			_direction *= -1

func _on_body_entered(_body):
	if _body is Player:
		_animation.play("fade_in")

func _on_body_exited(_body):
	if _body is Player:
		_animation.play("fade_out")
		#_message_baloon.hide()
