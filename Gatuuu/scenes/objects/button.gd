extends Area2D

signal button_pressed

@export var _cur_sprite: AnimatedSprite2D

func _on_body_entered(_body):
	if _body is Player:
		_cur_sprite.frame = 1
		button_pressed.emit()
