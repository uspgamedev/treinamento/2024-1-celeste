extends Area2D

@onready var _light_on := $PointLight2D

@export var _message_display: Label

# Liga a luz do outdoor quando o player passa
func _on_body_entered(body):
	if body.name == "Player":
			_light_on.show()
