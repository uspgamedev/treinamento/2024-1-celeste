extends StaticBody2D

@export var _platform_sprite: Sprite2D
@export var _speed: float = 50
@export var _platform_type: String
@export var _hor_distance: int
@export var _ver_distance: int

@onready var _initial_platform_pos_h = _platform_sprite.global_position.x
@onready var _initial_platform_pos_v =  _platform_sprite.global_position.y
@onready var _direction = 1

var _player_body: Player

func _process(delta):
	if _platform_type == "Horizontal":
		if _player_body:
			_player_body.global_position.x += _speed * _direction * delta
		
		global_position.x += _speed * _direction * delta
		if abs(global_position.x - _initial_platform_pos_h) > _hor_distance:
			_direction *= -1
	
	if _platform_type == "Vertical":
		global_position.y += _speed * _direction * delta
		if abs(global_position.y - _initial_platform_pos_v) > _ver_distance:
			_direction *= -1


func _on_detection_area_body_entered(body):
	if body is Player:
		_player_body = body

func _on_detection_area_body_exited(body):
	if body is Player:
		_player_body = null
