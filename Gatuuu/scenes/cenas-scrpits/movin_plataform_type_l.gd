extends Node2D
@onready var plataforma = $Plataforma

@onready var enviroment = $Plataforma/CollisionShape2D/TileMap/Enviroment

@export var offset = Vector2(0, 0)
@export var speed = 5.0
var tween

func _ready():
	enviroment.add_to_group("Plataforma_movel")
func start_tween():
	tween = get_tree().create_tween().set_process_mode(Tween.TWEEN_PROCESS_PHYSICS)
	tween.set_loops()
	tween.tween_property($Plataforma, "position", offset, speed)
	tween.tween_property($Plataforma, "position", Vector2.ZERO, speed)

func param(x,y,s):
	offset = Vector2(x,y)
	speed = s
func _process(_delta):
	if PlayerStatus.is_dead:
		tween.stop()
