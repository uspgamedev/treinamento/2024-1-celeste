extends Area2D

func _on_body_entered(body):
	if body.name == "Player":
		call_deferred("_process_body_entered", body)

func _process_body_entered(body):
	if body.name == "Player":
		PlayerStatus.is_dead = true
