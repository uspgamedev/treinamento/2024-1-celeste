extends CharacterBody2D
class_name Player

@onready var sprites = $Sprites
@onready var anim = $Animations
@onready var wall_detector_up = $Wall_detector_up
@onready var wall_detector_mid = $Wall_detector_mid
@onready var wall_detector_down = $Wall_detector_down
@onready var timer = $Timer

@export var _jump_sfx: AudioStreamPlayer2D

const TILE_MAP = preload("res://Sprites/Tile-map/tile_map.tscn")


const gravity = 340
var jump_force = PlayerStatus.jump_force

var fast_wall_jumping = false
var wall_jumping = false
var can_slide = true

var dash_timer = 0
var hold_timer = 0
var corner_jump_timer = 0
var frame = 0
var dash_direction = Vector2.ZERO
var direction = ""

var idle_timer = 0


func _ready():
	PlayerStatus.is_dead = false
	PlayerStatus.can_dash = true
	
	#self.position  = PlayerStatus.initial_pos
	#self.position = PlayerStatus.spawn_point # Muda conforme chegamos ao checkpoint
	PlayerStatus.death_count = 0
	PlayerStatus.fish_count = 0
	
func _physics_process(delta):
	if death():
		return
	
	get_direction()
	_floor(delta)
	check_wall(delta)
	walk_on_floor(delta)
	walk_on_air(delta)
	crounch(delta)
	dashing(delta)
	move_and_slide()


#================================================================================================
#================================================================================================
func reaparentar():
	var body1 = wall_detector_down.get_collider()
	var body2 = wall_detector_up.get_collider()
	var body
	if body1 != null:
		body = body1
	elif body2 != null:
		body = body2
	else:
		body = null
	
	if body != null and body.is_in_group("Plataforma_movel"):
		reparent(body) 
	else:
		reparent(SceneManager.cur_scene_node)
	
func check_wall(delta):
	# Verifica se existe uma parede que se pode segurar com Wall_detector em parte superior e parte inferior do Player
	var check_wall_up = wall_detector_up.is_colliding() and !wall_detector_up.get_collider().is_in_group("Enemy")
	var check_wall_mid = wall_detector_mid.is_colliding() and !wall_detector_mid.get_collider().is_in_group("Enemy")
	var check_wall_down = wall_detector_down.is_colliding() and !wall_detector_down.get_collider().is_in_group("Enemy")
	
	# Se Player já está realizando ação de wall_jump, saí desta função
	if wall_jumping or fast_wall_jumping:
		return
		
	# Checa se Player está tocando parede, e se está em uma das suas quinas
	if check_wall_up or check_wall_down:
		PlayerStatus.is_touching_wall = true
		if !check_wall_up:
			PlayerStatus.is_up_corner = true
		else:
			PlayerStatus.is_up_corner = false
			
		if !check_wall_down:
			PlayerStatus.is_down_corner = true
		else:
			PlayerStatus.is_down_corner = false
	else:
		PlayerStatus.is_touching_wall = false
		PlayerStatus.is_up_corner = false
		PlayerStatus.is_down_corner = false
		reparent(SceneManager.cur_scene_node)
	
	# Checa se Player está segurando a parede ou não
	if PlayerStatus.is_touching_wall and Input.is_action_pressed("key_grab") and PlayerStatus.can_hold and not PlayerStatus.is_dashing:
		PlayerStatus.is_holding = true
		reaparentar() # Tranfere Player como filho da plataforma, quando necessário
		idle_timer = 0
	else:
		PlayerStatus.is_holding = false

	# Executa ações enquanto jogador está segurando a parede
	if PlayerStatus.is_holding:
		# Checa o limite de tempo de segurar parede do Player
		hold_timer += delta
		if hold_timer >= 1:
			PlayerStatus.stamina -= 10
			hold_timer = 0
		if PlayerStatus.stamina <= 0:
			PlayerStatus.can_hold = false
		
		# Executa ação do Player de escalar a parede para cima
		if direction == "up" or direction == "up_left" or direction == "up_right":
			hold_timer += 1.2 * delta
			
			# Se Player está na quina superior da plataforma e está totalmente saído nele, próximo comando dele de subir ele sobe na plataforma
			if PlayerStatus.is_up_corner and not check_wall_mid:
				jump(false)
			
			# Caso contrário, executa a ação normal de descer na parede
			else:
				velocity.y = PlayerStatus.hiking_speed
				if PlayerStatus.can_dash: 
					anim.play("Climb")
				else:
					anim.play("Climb_pink")
				
		# Executa ação do Player de escorregar na parede para baixo
		elif direction == "down" or direction == "down_left" or direction == "down_right":
			# Setar a velocidade de descida 
			velocity.y = -PlayerStatus.hiking_speed
			
			# Executa a ação normal de descer na parede
			if PlayerStatus.can_dash: 
				# Se Player está na quina da plataforma, muda para sprite de balançar no ar
				if PlayerStatus.is_down_corner:
					anim.play("Climbing_down") #Change to "Swing" when we have it
				anim.play("Climbing_down")
			else:
				# Se Player está na quina da plataforma, muda para sprite de balançar no ar
				if PlayerStatus.is_down_corner:
					anim.play("Climbing_down_pink") #Change to "Swing" when we have it
				anim.play("Climbing_down_pink")
			
		# Executa ação do Player de ficar parado na parede
		elif direction == "null" or direction == "right" or direction == "left":
			# Se Player está na quina superior da plataforma mas não está saindo totalmente, escorrega de volta
			if PlayerStatus.is_up_corner:
				velocity.y = -PlayerStatus.hiking_speed
				if PlayerStatus.can_dash: 
					anim.play("Climbing_down")
				else:
					anim.play("Climbing_down_pink")
			else:
				velocity.y = 0
				if PlayerStatus.can_dash: 
					anim.play("Idle_Climb")
				else:
					anim.play("Idle_Climb_pink")
		
		
#================================================================================================
#================================================================================================

func _floor(delta):
	if	not is_on_floor() and not PlayerStatus.is_dashing and not PlayerStatus.is_holding:
		velocity.y += gravity * delta
		if velocity.y > -30 and velocity.y < 30:
			if PlayerStatus.can_dash: 
				anim.play("Jump_zero")
			else:
				anim.play("Jump_zero_pink")
		elif velocity.y < -30:
			if PlayerStatus.can_dash: 
				anim.play("Jump_up")
			else:
				anim.play("Jump_up_pink")
		else:
			if PlayerStatus.can_dash: 
				anim.play("Jump_down")
			else:
				anim.play("Jump_down_pink")
		
		if velocity.y >= 10 and fast_wall_jumping:
			fast_wall_jumping = false
			if PlayerStatus.stamina > 0:
				PlayerStatus.can_hold = true
		if velocity.y >= -23 and wall_jumping:
				wall_jumping = false
				if PlayerStatus.stamina > 0:
					PlayerStatus.can_hold = true
	if is_on_floor() and not PlayerStatus.is_dashing:
		PlayerStatus.can_dash = true
		PlayerStatus.can_hold = true
		hold_timer = 0
		PlayerStatus.stamina = 100



#================================================================================================
#================================================================================================


func death() -> bool:
	if PlayerStatus.is_dead:
		if PlayerStatus.can_dash:
			anim.play("Death")
		else: 
			anim.play("Death_pink")
		return true
	else:
		return false

#================================================================================================
#================================================================================================
		

func get_direction():
	var input = Vector2.ZERO
	input = Input.get_vector("key_left", "key_right", "key_up", "key_down")
	if input == Vector2.ZERO:
		direction = "null"
		return
	
	var angle = input.angle()

	if angle > -PI/8 and angle < PI/8:
		direction = "right"
	elif angle > PI/8 and angle < 3*PI/8:
		direction = "down_right"
	elif angle > 3*PI/8 and angle < 5*PI/8:
		direction = "down"
	elif angle > 5*PI/8 and angle < 7*PI/8:
		direction = "down_left"
	elif angle > 7*PI/8 or angle < -7*PI/8:
		direction = "left"
	elif angle > -7*PI/8 and angle < -5*PI/8:
		direction = "up_left"
	elif angle > -5*PI/8 and angle < -3*PI/8:
		direction = "up"
	elif angle > -3*PI/8 and angle < -PI/8:
		direction = "up_right"

#================================================================================================
#================================================================================================

func walk_on_floor(delta):
	if PlayerStatus.is_holding or PlayerStatus.is_dashing or PlayerStatus.is_crouching or not is_on_floor():
		return

	if direction == "right" or direction == "down_right" or direction == "up_right":
		velocity.x = PlayerStatus.speed
		sprites.flip_h = false
		wall_detector_up.scale.x = 1
		wall_detector_mid.scale.x = 1
		wall_detector_down.scale.x = 1
		anim.play("Run")
		idle_timer = 0
		
		PlayerStatus.direction = "right"
		
	elif direction == "left" or direction == "down_left" or direction == "up_left":
		velocity.x = -PlayerStatus.speed
		sprites.flip_h = true
		wall_detector_up.scale.x = -1
		wall_detector_mid.scale.x = -1
		wall_detector_down.scale.x = -1
		anim.play("Run")
		idle_timer = 0
		
		PlayerStatus.direction = "left"
		
	elif direction == "null" or direction == "up":
		
		PlayerStatus.direction = "null"
				
		if idle_timer < PlayerStatus.time_to_sit:
			idle_timer += delta
			anim.play("Idle")
		else:
			anim.play("Idle_sitting")
		velocity.x = 0
		
#================================================================================================
#================================================================================================

func _input(event):
	if event.is_action_pressed("key_jump"):
		idle_timer = 0
		jump(true)
	elif event.is_action_pressed("key_dash"):
		idle_timer = 0
		dash()
		
#================================================================================================
#================================================================================================

func walk_on_air(delta):
	if PlayerStatus.is_holding or PlayerStatus.is_dashing or PlayerStatus.is_crouching or is_on_floor():
		return
	
	if fast_wall_jumping:
		return
	
	# Parte que define ação de movimento horizontal do Player no corner_jump
	corner_jump_timer += delta
	if corner_jump_timer < PlayerStatus.corner_jump_duration:
		if wall_detector_up.scale.x == 1:
			velocity.x = PlayerStatus.corner_jump_speed
		elif wall_detector_up.scale.x == -1:
			velocity.x = -PlayerStatus.corner_jump_speed

	if direction == "right" or direction == "down_right" or direction == "up_right":
		if PlayerStatus.direction == "right" and wall_jumping:
			return
		velocity.x = PlayerStatus.speed
		sprites.flip_h = false
		wall_detector_up.scale.x = 1
		wall_detector_mid.scale.x = 1
		wall_detector_down.scale.x = 1
		
		if (wall_detector_up.is_colliding() or wall_detector_down.is_colliding()) and can_slide:
			velocity.y = -PlayerStatus.hiking_speed
			anim.play("Climbing_down")
			PlayerStatus.is_sliding = true
		else:
			can_slide = true
			PlayerStatus.is_sliding = false
		PlayerStatus.direction = "right"
		
	elif direction == "left" or direction == "down_left" or direction == "up_left":
		if PlayerStatus.direction == "left" and wall_jumping:
			return
		velocity.x = -PlayerStatus.speed
		sprites.flip_h = true
		wall_detector_up.scale.x = -1
		wall_detector_mid.scale.x = -1
		wall_detector_down.scale.x = -1
		
		if (wall_detector_up.is_colliding() or wall_detector_down.is_colliding()) and can_slide:
			velocity.y = -PlayerStatus.hiking_speed
			anim.play("Climbing_down")
			PlayerStatus.is_sliding = true
		else:
			can_slide = true
			PlayerStatus.is_sliding = false

		PlayerStatus.direction = "left"

	else:
		velocity.x = move_toward(velocity.x, 0, delta * PlayerStatus.speed * 2)
		PlayerStatus.direction = "null"

#================================================================================================
#================================================================================================

func _on_area_2d_body_entered(body):
	if body.is_in_group("Enemy"):
		PlayerStatus.is_dead = true

#================================================================================================
#================================================================================================
func jump(input):
	if PlayerStatus.is_dashing:
			return
	if is_on_floor() and not PlayerStatus.is_holding:
		velocity.y = jump_force
		anim.play("Jump_up")
		fumacinha()
		_jump_sfx.play()
		return
		
	# Define a ação da velocidade vertical do corner_jump se o player nao tiver apertado para pular
	if PlayerStatus.is_up_corner and not input:
		corner_jump_timer = 0
		velocity.y = PlayerStatus.corner_jump_force
		anim.play("Jump_up")
		return
		
	if PlayerStatus.is_touching_wall and not is_on_floor() and not PlayerStatus.is_holding:
		PlayerStatus.is_touching_wall = false
		fast_wall_jumping = true
		PlayerStatus.stamina -= 30

		if wall_detector_up.scale.x == 1:
			
			#Se nenhuma input estiver pressionada pula menos
			if direction == "null":
				velocity.y = PlayerStatus.weak_wall_jump_force
				velocity.x = -PlayerStatus.speed/1.5
			else:
				velocity.y = PlayerStatus.wall_jump_force
				velocity.x = -PlayerStatus.speed


			sprites.flip_h = true
			wall_detector_up.scale.x = -1
			wall_detector_mid.scale.x = -1
			wall_detector_down.scale.x = -1
		elif wall_detector_up.scale.x == -1:

			#Se nenhuma input estiver pressionada pula menos
			if direction == "null":
				velocity.y = PlayerStatus.weak_wall_jump_force
				velocity.x = PlayerStatus.speed/1.5
			else:
				velocity.y = PlayerStatus.wall_jump_force
				velocity.x = PlayerStatus.speed


			sprites.flip_h = false
			wall_detector_up.scale.x = 1
			wall_detector_mid.scale.x = 1
			wall_detector_down.scale.x = 1
		anim.play("Jump_up")
		return
	if PlayerStatus.is_holding:
		wall_jumping = true
		can_slide = false
		velocity.y = PlayerStatus.wall_jump_force
		PlayerStatus.is_holding = false
		PlayerStatus.can_hold = false
#================================================================================================
#================================================================================================
		
func dash():
	if not PlayerStatus.can_dash or PlayerStatus.is_dashing:
		return
	
	PlayerStatus.is_dashing = true
	PlayerStatus.can_dash = false
	
	timer.start()
	get_tree().paused = true

	dash_timer = 0
	frame = 0
	
	
	if direction == "null":
		if PlayerStatus.is_holding:
			sprites.flip_h = !sprites.flip_h
			wall_detector_up.scale.x = -wall_detector_up.scale.x
			wall_detector_mid.scale.x = -wall_detector_mid.scale.x
			wall_detector_down.scale.x = -wall_detector_down.scale.x
		if sprites.flip_h == false:
			dash_direction = Vector2(1,0)
		else:
			dash_direction = Vector2(-1,0)

			
	if direction == "right":
		dash_direction = Vector2(1,0)
	elif direction == "left":
		dash_direction = Vector2(-1,0)
	elif direction == "up":
		dash_direction = Vector2(0,-1)
	elif direction == "down":
		dash_direction = Vector2(0,1)
	elif direction == "up_right":
		dash_direction = Vector2(0.707,-0.707)
	elif direction == "up_left":
		dash_direction = Vector2(-0.707,-0.707)
	elif direction == "down_right":
		dash_direction = Vector2(0.707,0.707)
	elif direction == "down_left":
		dash_direction = Vector2(-0.707,0.707)
	velocity = dash_direction * PlayerStatus.dash_force

#================================================================================================
#================================================================================================

func dashing(delta):
	if not PlayerStatus.is_dashing:
		return
	add_dash_sprite()
	dash_timer += delta
	if dash_timer >= PlayerStatus.dash_duration:
		PlayerStatus.is_dashing = false
		velocity = Vector2(0, 0)
		sprites.visible = true
	else:
		return

#================================================================================================
#================================================================================================

func add_dash_sprite():
	var dash_sprite = preload("res://scenes/cenas-scrpits/Dash.tscn").instantiate()
	SceneManager.cur_scene_node.add_child(dash_sprite)
	#get_tree().current_scene.add_child(dash_sprite)
	dash_sprite.rotation = dash_direction.angle()
	
	dash_sprite.frame = frame
	frame += 1
	if dash_sprite.rotation > -PI/2 and dash_sprite.rotation <= 0:
		dash_sprite.flip_v = false
	elif dash_sprite.rotation < PI/2 and dash_sprite.rotation > 0:
		dash_sprite.flip_v = false
	elif dash_sprite.rotation < -PI/2: #
		dash_sprite.flip_v = true
	else:
		dash_sprite.flip_v = true
	
	dash_sprite.set_property(position)
#================================================================================================
#================================================================================================

func _on_timer_timeout():
	get_tree().paused = false
	timer.stop()
	sprites.visible = false
#================================================================================================
#================================================================================================

### Rever esse bloco
func _on_animations_animation_finished(anim_name):
	if anim_name == "Death" or anim_name == "Death_pink":
		self.global_position = PlayerStatus.spawn_point
		PlayerStatus.is_dead = false
		# Adiciona uma morte ao contador de mortes 
		PlayerStatus.death_count += 1 


#================================================================================================
#================================================================================================

func crounch(delta):
	if direction == "down" and is_on_floor():
		anim.play("Crounch")
		velocity.x = 0
		if jump_force > PlayerStatus.max_jump:
			jump_force -= 100*delta
	else:
		jump_force = PlayerStatus.jump_force

func fumacinha():
	var fumaca_sprite = preload("res://scenes/cenas-scrpits/fumaca_pulo.tscn").instantiate()
	fumaca_sprite.set_property(position)
	SceneManager.cur_scene_node.add_child(fumaca_sprite)
	fumaca_sprite.play()


