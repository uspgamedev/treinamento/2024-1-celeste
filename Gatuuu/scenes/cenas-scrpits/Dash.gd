extends AnimatedSprite2D

# Called when the node enters the scene tree for the first time.
func _ready():
	dashing()
	
func set_property(tx_pos):
	position = tx_pos
	
func dashing():
	var tween_fade = create_tween()
	tween_fade.tween_property(self, "self_modulate", Color(0.66505229473114, 0.6624464392662, 0.65534770488739, 0), 0.1)	
	await tween_fade.finished
	queue_free()
