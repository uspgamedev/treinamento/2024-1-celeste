extends CharacterBody2D

@onready var player = $"../Player"
@onready var anim = $Animation
@onready var collision_shape = $CollisionShape2D



func _ready():
	collision_shape.disabled = true
	#anim.play("Idle")


func equal(value,list):
	for num in list:
		if abs(value - num) <= 0.01:
			return true
	return false

func _on_area_2d_body_entered(body):
	if body.name == "Player":
		PlayerStatus.can_dash = true
		PlayerStatus.stamina = 100
		anim.play("Bounce")
		
		print(self.rotation_degrees)
		
		if equal(self.rotation_degrees,[0,-360,360]):
			player.velocity.y = -200
			player.anim.play("Jump_up")
			player.fumacinha()
			
		elif equal(self.rotation_degrees,[90,-270]):
			player.velocity.y = -100
			player.velocity.x = 200
			player.anim.play("Jump_up")
			player.fumacinha()
		
		elif equal(self.rotation_degrees,[180,-180]):
			player.velocity.y = 200
			player.anim.play("Jump_up")
			player.fumacinha()
		
		elif equal(self.rotation_degrees,[270,-90]):
			player.velocity.y = -100
			player.velocity.x = -200
			player.anim.play("Jump_up")
			player.fumacinha()
