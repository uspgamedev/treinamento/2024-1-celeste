extends Camera2D

const SCREEN_SIZE: Vector2 = Vector2(320, 180)
var _cur_screen: Vector2 = Vector2(0,0)

func _ready():
	set_as_top_level(true)
	_cur_screen = (get_parent().global_position / SCREEN_SIZE).floor()
	_update_screen(_cur_screen)

func _process(_delta):
	position_smoothing_enabled = true
	position_smoothing_speed = 5
	var parent_screen: Vector2 = (get_parent().global_position / SCREEN_SIZE).floor()
	if not parent_screen.is_equal_approx(_cur_screen):
		_update_screen(parent_screen)

func _update_screen(new_screen: Vector2):
	_cur_screen = new_screen
	global_position = _cur_screen * SCREEN_SIZE + SCREEN_SIZE * 0.5


#func param(left,right,top,bottom):
	#limit_left = left
	#limit_right = right
	#limit_top = top
	#limit_bottom = bottom
