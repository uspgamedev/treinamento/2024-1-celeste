extends Node2D

@onready var plataforma = $Plataforma

@export var speed = 1
@export var radius = 0

var angle_offset = 0
var new_position = Vector2.ZERO

func _process(delta):
	angle_offset += 2 * PI * delta /float(speed)

	new_position = Vector2(cos(angle_offset) * radius, sin(angle_offset) * radius)
	plataforma.position = new_position

func param(r,s):
	radius = r
	speed = s
