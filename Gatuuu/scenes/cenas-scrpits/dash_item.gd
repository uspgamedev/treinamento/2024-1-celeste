extends CharacterBody2D
@onready var anim = $Animation
@onready var collision_shape = $CollisionShape2D
@onready var timer = $Timer

func _ready():
	collision_shape.disabled = true
	anim.play("Default")

func _on_area_2d_body_entered(body):
	if body.name == "Player":
		PlayerStatus.can_dash = true
		PlayerStatus.stamina = 100
		anim.play("Collected")
		call_deferred("_disable_collision_shape")



func _on_animation_animation_finished(anim_name):
	if anim_name == "Collected":
		timer.start()
		$AnimatedSprite2D.visible = false

func _disable_collision_shape():
	get_node("Area2D/CollisionShape2D").disabled = true

func _enable_collision_shape():
	get_node("Area2D/CollisionShape2D").disabled = false

func _on_timer_timeout():
	timer.stop()
	$AnimatedSprite2D.visible = true
	anim.play("Default")
	call_deferred("_enable_collision_shape")
