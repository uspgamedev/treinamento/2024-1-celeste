extends Control

@export var _animation_pause: AnimationPlayer
@export var _resume_button: Button

func _ready():
	_animation_pause.play("RESET")
	self.visible = false
	


func _process(_delta):
	testEsc()

func resume():
	get_tree().paused = false
	_animation_pause.play_backwards("blur")
	self.visible = false

func pause():
	get_tree().paused = true
	_animation_pause.play("blur")
	self.visible = true

func testEsc():
	if Input.is_action_just_pressed("key_pause") and get_tree().paused == false:
		pause()
		_resume_button.grab_focus()
	elif Input.is_action_just_pressed("key_pause") and get_tree().paused == true:
		resume()

func _on_resume_pressed():
	resume()

func _on_restart_pressed():
	resume()
	SceneManager.change_to_game_screen()

func _on_quit_pressed():
	get_tree().quit()
