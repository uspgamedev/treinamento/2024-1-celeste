extends CanvasLayer
class_name HUD

@export var _label_text: Label

func _ready():
	PlayerStatus.game_start_time = Time.get_ticks_msec()

func _process(_delta):
	_label_text.text = PlayerStatus.get_time()


