extends Control

@export var _time_label: Label
@export var _death_label: Label
@export var _collectible_label: Label
@export var _restart_button: Button


func _ready():
	_time_label.text = str("Time Spent: ") + str(PlayerStatus.get_time())
	_death_label.text = str("Number of Deaths: ") + str(PlayerStatus.death_count)
	_collectible_label.text = str("Fish collected: ") + str(PlayerStatus.fish_count)
	_restart_button.grab_focus()

func _on_restart_button_pressed():
	SceneManager.change_to_game_screen()


func _on_title_screen_pressed():
	SceneManager.change_to_title_screen()
