extends Control

@export var _play_button: Button

func _ready():
	SceneManager.cur_scene_node = self
	_play_button.grab_focus()

func _on_play_pressed():
	SceneManager.change_to_game_screen()

func _on_quit_pressed():
	get_tree().quit()

func _on_credits_pressed():
	SceneManager.change_to_credits_screen()
