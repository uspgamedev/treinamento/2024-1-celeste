extends Area2D

@export var _animation: AnimationPlayer

func _on_body_entered(_body):
	if _body is Player:
		PlayerStatus.update_spawn(self.global_position)
		_animation.play("confirm_checkpoint")
