extends Area2D

func _on_body_entered(_body):
	if _body is Player:
		SceneManager.change_to_win_screen()
