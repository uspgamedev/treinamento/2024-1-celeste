extends MarginContainer

@export var _button: Button

func _ready():
	_button.grab_focus()

func _on_button_pressed():
	SceneManager.change_to_title_screen()
