extends Control

@onready var play_button = $VBoxContainer/Play_button

func _ready():
	SceneManager.cur_scene_node = self
	play_button.grab_focus()

func _on_play_button_pressed():
	SceneManager.change_to_game_screen()

func _on_quit_button_pressed():
	get_tree().quit()
