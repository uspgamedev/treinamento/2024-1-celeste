extends Node

const hiking_speed = -20
const wall_jump_force = -135
const weak_wall_jump_force = -95
const jump_force = -110.0
const speed = 80

# Constantes e variáveis usados para definir a ação do dash
const dash_duration = 0.07
const dash_force = 600
var is_dashing = false
var can_dash = false

# Constantes e variáveis usados para definir a ação do corner da plataforma
const corner_jump_duration = 0.07
const corner_jump_speed = 40
const corner_jump_force = -70.0
var is_up_corner = false
var is_down_corner = false

# Constante e variáveis usados para define a ação de hold
const cannot_hold_duration = 0.1
var can_hold = false
var is_holding = false

var is_on_movel_platform = false
var time_to_sit = 3
var stamina = 100
var is_dead = false
var max_jump = -160
var is_crouching = false
var is_touching_wall = false
var is_sliding = true

var direction = ""

# Para lidar com os spawn points ao longo da fase
@onready var initial_pos: Vector2 = Vector2(20, 247)
var spawn_point = initial_pos # Valor inicial. Tanto faz.
# Função para definir nova posição do player, quando chega no checkpoint
func update_spawn(new_point):
	spawn_point = new_point

# SpeedRun Timer
var game_start_time
func get_time():
	var current_time = Time.get_ticks_msec() - game_start_time
	var time_in_min = current_time / 1000 / 60
	var time_in_sec = current_time / 1000 % 60
	var time_in_msec = current_time % 1000 / 10
	if time_in_min < 10:
		if time_in_min == 0:
			time_in_min = "00"
		else:
			time_in_min = "0" + str(time_in_min)
	
	if time_in_sec < 10:
		if time_in_sec == 0:
			time_in_sec = "00"
		else:
			time_in_sec = "0" + str(time_in_sec)
	
	if time_in_msec < 10:
		if time_in_msec == 0:
			time_in_msec = "00"
		else:
			time_in_msec = "0" + str(time_in_msec)
	
	return (str(time_in_min) + ":" + str(time_in_sec) + ":" + str(time_in_msec))

# Variável para guardar quantas vezes o player morreu
var death_count: int = 0

# Variáveis para os coletáveis
var fish_count: int = 0
