extends Node

@export var _title_screen: PackedScene
@export var _game_screen: PackedScene
@export var _win_screen: PackedScene
@export var _credits_screen: PackedScene

var cur_scene_node: Node

func change_scene(scene_root: Node):
	cur_scene_node.queue_free()
	await cur_scene_node.tree_exited
	cur_scene_node = scene_root
	get_tree().root.add_child(scene_root)

func change_to_title_screen():
	change_scene(_title_screen.instantiate())

func change_to_win_screen():
	change_scene(_win_screen.instantiate())

func change_to_game_screen():
	change_scene(_game_screen.instantiate())

func change_to_credits_screen():
	change_scene(_credits_screen.instantiate())
